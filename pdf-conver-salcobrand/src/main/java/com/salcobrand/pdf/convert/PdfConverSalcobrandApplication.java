package com.salcobrand.pdf.convert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PdfConverSalcobrandApplication {

	public static void main(String[] args) {
		SpringApplication.run(PdfConverSalcobrandApplication.class, args);
	}

}
